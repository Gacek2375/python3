# Thin function is similar to script with argv
def print_two(*args):
    arg1, arg2 = args
    print(f"arg1: {arg1}, arg2: {arg2}")

# OK, then args is unnecessary
def print_two_again(arg1, arg2):
    print(f"arg1: {arg1}, arg2: {arg2}")

# This function takes one argument
def print_one(arg1):
    print(f"arg1: {arg1}")

# This function don't takes argument
def print_none():
    print("I haven't got anything'")

print_two("Ulther", "Ego")
print_two_again("Ulther", "Ego")
print_one("Ulther")
print_none()
class Song(object):

    def __init__(self, lyrics):
        self.lyrics = lyrics

    def sing_me_a_song(self):
        for line in self.lyrics:
            print(line)
        print("\n")

frontside = Song(["Flower of love is blossoming in the light of feelings",
                    "And here it dies too",
                    "I know it!",
                    "When the time will come take the last journey,",
                    "I'll be thinking of you..."])

hunter = Song(["You're the king of the world",
                "In your little dream",
                "Here the irreversible",
                "Changes into alive"])

the_doors = Song(["Riders on the storm.",
                    "Riders on the storm.",
                    "Into this house we're bornInto this world we're thrown."])

frontside.sing_me_a_song()
hunter.sing_me_a_song()
the_doors.sing_me_a_song()

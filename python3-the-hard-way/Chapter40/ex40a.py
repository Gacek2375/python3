import mystuff

# Definition in mystuff.py
mystuff.apple()
# Veriable in mystuff.py
print(mystuff.tangerine)

class MyStuff(object):

    def __init__(self):
        self.tangerine = "And now a thousand years between"

    def apple(self):
        print("I'm apple with class!")

thing = MyStuff()
thing.apple()
print(thing.tangerine)

# in dicitionary style
mystuff['apples']

# in a modules style
mystuff.apples()
print(mystuff.tangerine)

# in a class style
thing = MyStuff()
thing.apples()
print(thing.tangerine)


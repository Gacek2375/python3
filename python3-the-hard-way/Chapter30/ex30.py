people = 30
cars = 15
trucks = 40


if cars > people:
    print("We should go by car.")
elif cars < people:
    print("We shouldn't go by car.")
else:
    print("We can't decide.")

if trucks > cars:
    print("There are too many trucks.")
elif trucks < cars:
    print("Maybe we should take trucks.")
else:
    print("We still can't decide.")

if people > trucks:
    print("Alright, let's just take the trucks.")
else:
    print("OK, then we stay at home.")

if people > cars and trucks:
    print("There are too many of us.")
elif people < cars and trucks:
    print("We can't decide what to go.")
elif people == cars and trucks:
    print("Let's take a car and trucks,")
else:
    print("OK, then we stay at home.")

if cars or trucks > people:
    print("We can go what we want.")
elif cars or trucks < people:
    print("We're going on foot.")
else:
    print("Eh... stay at home.")

if cars > people or trucks < cars:
    print("We're going by car.")
elif cars < people or trucks > cars:
    print("Let's take a trucks.")
else:
    print("Again... stay at home.")

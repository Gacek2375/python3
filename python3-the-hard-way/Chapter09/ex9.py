days = "Mon Tue Wed Thu Fri Sat Sun"
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

print("These are the days of the week: ", days)
print("Here are the months: ", months)

print("""
    Something is going on here.
We use three double quotation marks.
We will be able to enter as much text as you like.
""")
# Komentarz; ułatwia póżniejsze czytanie programu.
# Wszystko co znajduje się po znaku X jest przez Pythona ignorowane.

print("I could write this code.") # a komentarz po kodzie gðzie ignorowany

# Komentarz może również użyć do 'wyłączenia', czyli wykomentowania kodu.
# print("It will not start.")

print("It will start.")

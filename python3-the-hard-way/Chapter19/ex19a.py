def cheese_and_crackers(Cheese_count, boxes_of_crackers):
    print(f"You have {Cheese_count} pieces of cheese!")
    print(f"You have {boxes_of_crackers} box crackers!")
    print("Dude, that's enough to make a party!")
    print("Organize a blanket.\n")


print("We can simply give the number function directly:")
cheese_and_crackers(20, 30)


print("Or we can use the variables from the script:")
amount_of_cheese = 10
amount_of_crackers = 50

cheese_and_crackers(amount_of_cheese, amount_of_crackers)


print("We can also do arithmetic operations inside:")
cheese_and_crackers(10 + 20, 5 + 6)


print("And we can combine these two things, i.e. variables and arithmetic operations:")
cheese_and_crackers(amount_of_cheese + 100, amount_of_crackers + 1000)

print("Transfer input to function: ")
input_of_cheese = int(input("Cheese: "))
input_of_crackers = int(input("Crackers: "))
cheese_and_crackers(input_of_cheese, input_of_crackers)

print("Or transfer input to function and arithmetic operations: ")
cheese_and_crackers(input_of_cheese + 10000, input_of_crackers + 100)

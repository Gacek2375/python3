def add(a, b):
    print(f"Addition {a} + {b}")
    return a + b

def subtract(a, b):
    print(f"Subtraction {a} - {b}")
    return a - b

def multiply(a, b):
    print(f"Multiplication {a} * {b}")
    return a * b

def divide(a, b):
    print(f"Division {a} / {b}")
    return a / b

print("Let's do some arithmetic operations using only the function!")

age = add(30, 5)
height = subtract(78, 4)
weight = multiply(90, 2)
iq = divide(100, 2)

print(f"Age: {age}, Height: {height}, Weight: {weight}, IQ: {iq}")

print("This is the task.")

what = add(age, subtract(height, multiply(weight, divide(iq, 2))))

print("This gives: ", what, "Would you be able to calculate it manually?")

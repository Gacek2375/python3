def add(a, b):
    print(f"Addition {a} + {b}")
    return a + b

def subtract(a, b):
    print(f"Subtraction {a} - {b}")
    return a - b

def multiply(a, b):
    print(f"Multiplication {a} * {b}")
    return a * b

def divide(a, b):
    print(f"Division {a} / {b}")
    return a / b

age = add(2, 2)
height = subtract(2, 2)
weight = multiply(2, 2)
iq = divide(2, 2)

print(f"Age: {age}, Height: {height}, Weight: {weight}, IQ: {iq}")

print("Task: ")

what = multiply(age, multiply(height, add(weight, subtract(iq, 2))))

print("This gives: ", what)

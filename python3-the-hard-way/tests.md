## Virtualenv
## pytest
## nosetests

1. pip install virtualenv nose pytest.
2. '__init__.py' in NAME and tests catalogs.
3. pytest.ini:
    
    [pytest]
python_files = *_tests.py tests.py tests_*.py 
addopts = -s -q --disable-warnings --doctest-modules
norecursedirs = .git .cache tmp*


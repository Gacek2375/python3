tabby_cat = "\tI'm tab."
persian_cat = "I'm a line\nbreak."
backslash_cat = "I'm \\ some \\ a cat."

fat_cat = """
Make a list:
    \t* Cat food
    \t* Fish
    \t* Catnip\n\t* Grass
"""

print(tabby_cat)
print(persian_cat)
print(backslash_cat)
print(fat_cat)
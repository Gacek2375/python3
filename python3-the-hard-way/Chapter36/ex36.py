from sys import exit

def gold_room():
    print("Pokój jest pełen złota ile zabierasz??")

    choice = int(input("> "))
    if choice <= 50:
        print("Ładnie, nie jesteś chciwy! Wygrałeś!")
        exit(0)
    elif choice >= 50:
        dead("Ty chciwy draniu!")
    else:
        dead("Palancie, naucz wpisywać się cyfry...")


def bear_room():
    print("""W pomieszczeniu znajduje się Pingwin.\nWidzisz że wysiaduje jajo.\nZa pingwinem znajdują się następne drzwi.
\nJak przesuniesz pingwina?
\t1. Zabieram jajo.
\t2. Przesuwam pingwina.
\t3. Otwieram drzwi.
""")
    
    tux = False

    while True:
        choice = input("> ")

        if choice == "zabieram jajo":
            dead("Pingwin zdziela Ci w pysk!")
        elif choice == "przesuwam pingwina" and not tux:
            print("Pingwin się przesuwa, co teraz?")
            tux = True
        elif choice == "przesuwam pingwina" and tux:
            dead("Pingwin się wkurwił, broniąc jaja odgryza Ci nos!")
        elif choice == "otwieram drzwi" and tux:
            gold_room()
        else:
            print("Nie wiem co masz na myśli (wybierz z listy).")



def cthulhu_room():
    print("""Znajdujesz się w ciemnym pomieszczeniu, widzisz tylko ogromne oko.\n
Oko Cthulhu, które zrzera Cię od środka.
\t1. uciekam
\t2. odgryzam sobie głowę
""")

    choice = input("> ")

    if "uciekam" in choice:
        start()
    elif "odgryzam sobie głowę" in choice:
        dead("To było smaczne!")
    else:
        cthulhu_room()




def dead(why):
    print(why, "Dobra robota!")
    exit(0)

def start():
    print("""Znajdujesz się w ciemnym pomieszczeniu. Nie ma tutaj nic poza zamkniętymi drzwiami.\n   
Które wybierasz?
\t1. Lewe.
\t2. Prawe.
""")
    
    choice = input("> ")

    if choice == "lewe":
        bear_room()
    elif choice == "prawe":
        cthulhu_room()
    else:
        dead("Błąkasz się po pomieszczeniu i nie znajdujesz innego wyjścia. Umierasz z głodu.")

start()

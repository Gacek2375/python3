# Logical Expressions


| **NOT**    | **TRUE?**  |
|------------|------------|
| not False  | True       |
| not True   | False      |


| **OR**         | **TRUE?**   |
|----------------|-------------|
| True or false  | True        |
| True or True   | True        |
| False or True  | True        |
| False or False | False       |

| **AND**        | **TRUE?**   |
|----------------|-------------|
| True and False | False       |
| True and True  | True        |
| False and True | False       |
| False and False| False       |


| **NOT OR**          | **True?**   |
|---------------------|-------------|
| not(True or False)  | False       |
| not(True or True)   | False       |
| not(False or True)  | False       |
| not(False or False) | True        |


| **NOT AND**          | **True?**   |
|----------------------|-------------|
| not(True and False)  | True        |
| not(True and True)   | False       |
| not(False and True)  | True        |
| not(False and False) | True        |

print("We practice everything.")
print("You need to practice the sequences of the little tooth with the \\, character that they insert: ")
print("\n new line and \t tabs.")

poem = """
\tThis beautiful world
with so much embedded logic
can't see the \n need for love
or to understand the passion of premmonition
and needs clarification
\n\tbut there are none.
"""

print("----------")
print(poem)
print("----------")


five = 10 - 2 + 3 - 6
print(f"It should be five: {five}")

def secret_formula(started):
    jelly_beans = started * 500
    jars = jelly_beans / 1000
    crates = jars / 100
    return jelly_beans, jars, crates


start_point = 10000
beans, jars, crates = secret_formula(start_point)

# Remember that this is another way to format a string
print("We start from the initial value: {}".format(start_point))
# This works similar to the string f""
print(f"It will give us jelly {beans}, {jars} jars and {crates} crates.")

sort_point = start_point / 10

print("We can also do it this way:")
formula = secret_formula(start_point)
# This is a simple way to apply a list to a formatted character string
print("It will give us {} jelly, {} jars and {} crates.".format(*formula))

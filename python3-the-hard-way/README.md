**Python 3 The Hard Way**
--------------------------
--------------------------

1. [Chapter01](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter01/) - First Program. 
2. [Chapter02](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter02/) - Comments.
3. [Chapter03](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter03/) - Algebraic numbers and operations.
4. [Chapter04](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter04/) - Variables and names.
5. [Chapter05](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter05/) - More Variables and printing.
6. [Chapter06](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter06/) - Strings and text.
7. [Chapter07](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter07/) - More printing.
8. [Chapter08](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter08/) - Printing, printing...
9. [Chapter09](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter09/) - Printing, printing, printing...
10. [Chapter10](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter10/) - What was that?
11. [Chapter11](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter11/) - Asking questions.
12. [Chapter12](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter12/) - Displaying user hints.
13. [Chapter13](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter13/) - Unpacked and variable parameters.
14. [Chapter14](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter14/) - Prompt and passing variable.
15. [Chapter15](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter15/) - Reading from files.
16. [Chapter16](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter16/) - Reading and writing files.
17. [Chapter17](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter17/) - More files.
18. [Chapter18](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter18/) - Names, variables, code and funcions.
19. [Chapter19](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter19/) - Functions and variables.
20. [Chapter20](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter20/) -
Functions and file.
21. [Chapter21](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter21/) - Functions and return.
22. [Chapter22](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter22/) - Lexicon.
23. [Chapter23](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter23/) - Decoding and Encoding laguages.
24. [Chapter24](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter24/) -
Practice with functions.
25. [Chapter25](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter25/) - Practice with functions and inside the intepreter.
26. [Chapter26](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter26/) - Test.
27. [Chapter27](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter27/logical_expressions.md) - Logical Expressions.
28. [Chapter28](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter28/example_logical.md) - Example Logical Expressions.
29. [Chapter29](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter29/) - Condition Instructions IF.
30. [Chapter30](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter30/) - Condition Instructions IF - ELSE - ELIF.
31. [Chapter31](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter31/) - IF - ELSE - ELIF -- GAMES.
32. [Chapter32](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter32/) - Lits and Loop FOR.
33. [Chapter33](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter33/) - WHILE LOOP.
34. [Chapter34](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter34/) - Index in List.
35. [Chapter35](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter35/) - Games whit if, function.
36. [Chapter36](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter36/) - Games.
37. [Chapter38](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter38/) - List Operation.
38. [Chapter39](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter39/) - Dicitionary.
38. [Chapter40](https://bitbucket.org/Gacek2375/python3/src/master/python3-the-hard-way/Chapter40/) - Class.


cars = 100
space_in_a_car = 4.0
drivers = 30
passengers = 90
cars_not_driven = cars - drivers
cars_driven = drivers
carpool_capacity = cars_driven * space_in_a_car
average_passengers_per_car = passengers / cars_driven

print(cars, "cars are available.")
print(drivers, "drivers are available")
print("Today there will be", cars_not_driven, "empty cars.")
print("Today we cant transport", carpool_capacity, "people.")
print("Today we have", passengers, "to transport.")
print("We need to put an avarage of", average_passengers_per_car, "people in each car.")

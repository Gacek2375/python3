from sys import argv

script, filename = argv

print(f"Clearing {filename}.")
print("If you don't want to do this, press CTRL+C (^C).")
print("If you want this, press RETURN.")

input("?")

print("Open file...")
target = open(filename, 'w+')

print("Now I will ask you to enter 3 lines of text.")
line1 = input("Line 1: ")
line2 = input("Line 2: ")
line3 = input("Line 3: ")

print("Writes them to a file.")
target.write(f"{line1}\n{line2}\n{line3}\n")

print("Reading File: ")
target.seek(0)
target.flush()
print(target.read(), end="")

print("And finally we close the file.")
target.close()

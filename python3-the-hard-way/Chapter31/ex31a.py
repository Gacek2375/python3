print("""Stoisz na rozdrożu dwóch dróg, co robisz?
        1. Droga w lewo.
        2. Droga w prawo.""")

droga = input("> ")

if droga == "1":
    print("""Droga krótka, choć prowadzi przez kamienie, a pod kamieniami leży szkło. Gdyby nie to szkło to by się szło... Na koncu drogi znajduje się stara, zaniedbana Chatka.
            1. Wchodzisz do chatki.
            2. Idziesz za chatkę by sprawdzić czy nie ma dalszej drogi.""")

    chatka = input("> ")

    if chatka == "1":
        print("""W Chatce panuje mrok, nie masz przy sobie żadnej latarki ani zapałek. Stawiasz krok w do przodu i wpadasz do piwnicy. Złamałe nogę, w okolicy nikogo nie ma. Giniesz z głodu!""")
    elif chatka == "2":
        print("""Za Chatką nie ma żadnej drogi, lecz widzisz w oddali jasny punkt, który Cię intryguje.
Podchodzisz do niego, lecz jego jasnosć oślepia Cię zbyt mocno. 
Próbujesz go dotknąć, gdy nagle wchłania Cię, cząstkę, po cząstce. 
Trafiasz w dziwne miejsce, gdzie nie dociera żaden dżwięk ani obraz, nie czuć zapachu, ani dotyku. 
Ogarnia Cię strach zarazem, spokój. Pozostajesz w tym świecie na zawsze.""")
        print("Gratulacje, wygrałęś!")

elif droga == "2":
    print("""Droga prowadzi przez asfaltową drogę. Idzie się gładko i spokojnie. Nucisz sobie piosenkę 'It's my life' i przypominasz koncert, na którym zgubiłeś swój stary, ulubiony portfel z milionem złotych. 
o jak wtedy się wściekleś, chciałeś wtedy przetrzepać całą salę.""")
    print("W zamyśleniu zapominasz gdzie miałeś dotrzeć. Zgubiłeś się jeszcze bardziej. Umierasz z wycięczenia i głodu. Gratulacje!")

else:
    print("Wpadasz w panikę. Biegasz jak popażony po lesie, pytkasz sie i głową upadasz w sidła na niedźwiedzie. Gratulacje!")

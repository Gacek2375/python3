print("""You enter a drk room with two doors.
        You go through door number 1 or 2?""")

door = input("> ")

if door == "1":
    print("You see a big bear there eating a cheesecake.")
    print("What are you doing?")
    print("1. You take the cheesecake.")
    print("2. You're screeming at the bear.")

    bear = input("> ")

    if bear == "1":
        print("The bear bites your nose. Good job!")
    elif bear == "2":
        print("The bear bites your legs off. Good job!")
    else:
        print(f"Well, {bear} it's probably a better choice.'")
        print("The bear runs away.")

elif door == "2":
    print("You stare into the infinite abyss of Cuthulu's eye,")
    print("1.Blueberries.")
    print("2.Yellow clothes pegs.")
    print("3.Gorged revolvers chant melodies.")

    insanity = input("> ")

    if insanity == "1" or insanity == "2":
        print("Your body is crazy but you have a brain like fruit jelly.")
        print("Good job!")
    else:
        print("They turn eyes crazy and turn into a puddle of mud.")
        print("Good job!")

else:
    print("You stumble, hope for a knife and die. Good job!")

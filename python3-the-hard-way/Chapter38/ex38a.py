ten_things = "Apples Orange Crows Telephone Light Sugar"

print("Whait, there are 10 items on this list. Let's fix it.")


stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Song", "Frisbee", "Maize", "Banana", "Woman", "Man"]

for i in range(len(stuff), 10):
    next_one = more_stuff.pop()
    print("Adding: ", next_one)
    stuff.append(next_one)
    print(f"Now is {len(stuff)} elements.")

print("Now have it", stuff)

print("Let's do some more things from staff.")

print(stuff[1])
print(stuff[-1])
print(stuff.pop())
print(' '.join(stuff))
print('#'.join(stuff[3:5]))

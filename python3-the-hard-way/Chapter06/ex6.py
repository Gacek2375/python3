types_of_people = 10
x = f"There are {types_of_people} types of people."

binary = "binary"
do_not = "don't know'"
y = f"Those who know the {binary} system and those who {do_not} it."

print(x)
print(y)

print(f"I said: {x}")
print(f"I also said: '{y}'")

hilarious = False

joke_evaluation = "Is this not a joke?! {}"

print(joke_evaluation.format(hilarious))

w = "This is the left side..."
e = "Character chain with right side."

print(w + e) 

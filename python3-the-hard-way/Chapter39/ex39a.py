# Wojwewództwo
wojewodztwo = {
        'Dolnośląskie': 'DL',
        'Kujawsko-Pomorskie': 'KP',
        'Lubelskie': 'LL',
        'Lubuskie': 'LS',
        'Łódzkie': 'ŁD',
        'Małopolskie': 'MP',
        'Mazowieckie': 'MZ',
        'Opolskie': 'OP',
        'Podkarpackie': 'PK',
        'Podlaskie': 'PL',
        'Pomorskie': 'PM',
        'Śląskie': 'ŚL',
        'Świętokrzyskie': "ŚK",
        'Warmińsko-Mazurskie': 'WM',
        'Wielkopolskie': 'WP',
        'Zachodnipomorskie': "ZP"
}

# Miasta
miasto = {
        'DL': 'Wrocław',
        'KP': 'Bydgoszcz',
        'LL': 'Lublin',
        'LS': 'Zielona Góra',
        'ŁD': 'Łódź',
        'MP': 'Kraków',
        'MZ': 'Warszawa',
        'OP': 'Opole',
        'PK': 'Rzeszów',
        'PL': 'Białystok',
        'PM': 'Gdańsk',
        'ŚL': 'Katowice',
        'ŚK': 'Kielce',
        'WM': 'Olsztyn',
        'WP': 'Poznań',
        'ZP': 'Szczecin'
}

print("""Sprawdź jakie miasto jest stolicą danego województwa.
Lista województw: 
""")

for wojewodztwo, skrot in list(wojewodztwo.items()):
    print(f"{wojewodztwo} ma skrót: {skrot}")


choice = input("\nPodaj Województwo: ")

if choice == 'DL' or choice == 'Dolnośląskie':
    print(miasto['DL'])
elif choice == 'KP' or choice == 'Kujawsko-Pomorskie':
    print(miasto['KP'])
elif choice == 'LL' or choice == 'Lubelskie':
    print(miasto['LL'])
elif choice == 'LS' or choice == 'Lubuskie':
    print(miasto['LS'])
elif choice == 'ŁD' or choice == 'Łódzkie':
    print(miasto['ŁD'])
elif choice == 'MP' or choice == 'Małopolskie':
    print(miasto['MP'])
elif choice == 'MZ' or choice == 'Mazowieckie':
    print(miasto['MZ'])
elif choice == 'OP' or choice == 'Opolskie':
    print(miasto['KP'])
elif choice == 'PK' or choice == 'Podkarpackie':
    print(miasto['PK'])
elif choice == 'PL' or choice == 'Podlaskie':
    print(miasto['PL'])
elif choice == 'PM' or choice == 'Pomorskie':
    print(miasto['PM'])
elif choice == 'ŚL' or choice == 'Śląskie':
    print(miasto['ŚL'])
elif choice == 'ŚK' or choice == 'Świętokrzyskie':
    print(miasto['ŚK'])
elif choice == 'WM' or choice == 'Warmińsko-Pomorskie':
    print(miasto['WM'])
elif choice == 'WP' or choice == 'Wielko Polskie':
    print(miasto['WP'])
elif choice == 'ZP' or choice == 'Zachodniopomorskie':
    print(miasto['ZP'])
else:
    print("Nie ma takiego miasta")


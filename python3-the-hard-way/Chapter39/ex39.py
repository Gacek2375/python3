# Maping name states to abbrev.
states = {
        'Oregano': 'OR',
        'Florida': "FL",
        'California': 'CA',
        'New York': 'NY',
        'Michigan': 'MI'
}

# The basic set of states and their cities.
cities = {
        'CA': 'San Francisco',
        'MI': 'Detroit',
        'FL': 'Jacksonville'
}

# add some cities.
cities['NY'] = 'New York'
cities['OR'] = 'Portland'

# printing some cities.
print('-' * 10)
print("State NY has: ", cities['NY'])
print("State OR has: ", cities['OR'])

# printing some states
print('-' * 10)
print("Abbrev for Michigan it: ", states['Michigan'])
print('Abbrev for Florida it: ', states['Florida'])

# First use the state dicitionary, then cities
print('-' * 10)
print("Michigan has: ", cities[states['Michigan']])
print("Florida has: ", cities[states['Florida']])

# printing abbrev each states
print('-' * 10)
for state, abbrev in list(states.items()):
    print(f"{state} has abbrev {abbrev}")

# printing any city in state
print('-' * 10)
for abbrev, city in list(cities.items()):
    print(f"{abbrev} has city {city}")

# both at once
print('-' * 10)
for state, abbrev in list(states.items()):
    print(f"State {state} hase abbrev {abbrev}")
    print(f"and city {cities[abbrev]}")

print('-' * 10)
# charge the abbrev directly by the state name, which may not exist
state = states.get('Texas')

if not state:
    print("Sorry, no state Texas.")

# retrive the city using default value
city = cities.get('TX', 'Does not exist')
print(f"City for the state 'TX' is: {city}")

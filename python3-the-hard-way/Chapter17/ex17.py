from sys import argv
from os.path import exists

script, from_file, to_file = argv

print(f"Copy from  {from_file} to {to_file}")

in_file = open(from_file)
indata = in_file.read()

print(f"Third input file has {len(indata)} bytes.")

print(f"Is the input file exist? {exists(to_file)}")
print("Press ENTER to continue, or CTRL+C to cancel.")

out_file = open(to_file, 'w')
out_file.write(indata)

print("OK, done.")

out_file.close()
in_file.close()

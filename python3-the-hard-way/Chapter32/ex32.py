the_count = [1, 2, 3, 4, 5]
fruits = ['apples', 'oranges', 'pears', 'apricots']
change = [1, 'onepeas', 2, 'twopeas', 3, 'fivepeas']

# This is first loop for, witch goes through the list.
for number in the_count:
    print(f"This is number: {number}")

# Same as above
for fruit in fruits:
    print(f"Type of fruit: {fruit}")

# We can also go through mmixed lists
for i in change:
    print(f"Have: {i}")


# We can also make lists; We start from empty
elements = []

# The we use the range function to count from 0 to 5
for i in range(0, 6):
    print(f"Adding {i} to this list.")
    # append is function the letters understand
    elements.append(i)

# Now we can also print them
for i in elements:
    print(f"This element was: {i}")


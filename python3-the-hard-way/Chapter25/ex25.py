def break_words(stuff):
    """This functions breaks a sentence into words."""
    words = stuff.split(' ')
    return words

def sort_words(words):
    """Sort words."""
    return sorted(words)

def print_first_word(words):
    """Printing the first word and removes it from the sentence."""
    word = words.pop(0)
    print(word)

def print_last_word(words):
    """Printing the last word and removes it from the sentence."""
    word = words.pop(-1)
    print(word)

def sort_sentence(sentence):
    """Gets the full sentence and returns the sorted words."""
    words = break_words(sentence)
    return sort_words(words)

def print_first_and_last(sentence):
    """Printing the first and last word of the sentence."""
    words = break_words(sentence)
    print_first_word(words)
    print_last_word(words)

def print_first_and_last_sorted(sentence):
    """Sorted the words and the prints the firt and last ones."""
    words = sort_sentence(sentence)
    print_first_word(words)
    print_last_word(words)
